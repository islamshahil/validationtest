import sys
import os
import xml.etree.ElementTree as ET


def xml_validation(file):

    try:
        tree = ET.parse(file)
        file = tree.getroot()
        res = ("Good "+str(file))
    except Exception as e:
        res = ("Error in "+str(file)+" : "+str(e))

    return (res)


if(__name__ == '__main__'):

    rootdir = "data"
    final_res =[]
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            filename = (os.path.join(rootdir, file))
            if(filename.find(".xml")!= -1):
                res = xml_validation(filename)
                final_res.append(res)

    for i in final_res:
        if(i.find("Error")!=-1):
            print(i)
            sys.exit(1)
        else:
            pass

    sys.exit(0)