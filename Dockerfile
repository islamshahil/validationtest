FROM python:3.8

WORKDIR /validationtest

COPY main.py .
COPY data .
COPY requirements.txt .

RUN pip install -r requirements.txt

CMD ["python", "main.py"]
